﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    public class WallCollider
    {
        public int X = 0;
        public int Y = 0;
        public int Height = 0;
        public int Width = 0;
    }
}
