﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    class EnemyType
    {
        public int speed;
        public int power;
        public string animationJson;
        public int health;
    }
}
