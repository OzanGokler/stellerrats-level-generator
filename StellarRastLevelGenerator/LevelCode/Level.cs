﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    public class Level
    {
        public string sDescription;

        public List<MapGrounds> Grounds;
        public List<MapWalls> Walls;
        public List<MapEnemyBase> EnemyBases;
        public List<WallCollider> WallColliders;
        public List<DoorCollider> DoorColliders;
        public List<MapTransporter> Transporters;
        public List<MapHealth> Healths;
        public List<MapComputer> Computer;
        public List<MapDoors> Doors;

    }
}
