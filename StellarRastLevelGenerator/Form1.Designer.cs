﻿namespace StellarRastLevelGenerator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LevelScreen = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Menu = new System.Windows.Forms.MenuStrip();
            this.File = new System.Windows.Forms.ToolStripMenuItem();
            this.LoadTextures = new System.Windows.Forms.ToolStripMenuItem();
            this.ExportJson = new System.Windows.Forms.ToolStripMenuItem();
            this.listView = new System.Windows.Forms.ListView();
            this.tbxLevelName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbxTileType = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbxCollider = new System.Windows.Forms.Label();
            this.tbxRow = new System.Windows.Forms.TextBox();
            this.tbxColumn = new System.Windows.Forms.TextBox();
            this.Row = new System.Windows.Forms.Label();
            this.Column = new System.Windows.Forms.Label();
            this.btnCreateLevel = new System.Windows.Forms.Button();
            this.TileLabel = new System.Windows.Forms.Label();
            this.tbxTile = new System.Windows.Forms.TextBox();
            this.butStartColliders = new System.Windows.Forms.Button();
            this.Menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // LevelScreen
            // 
            this.LevelScreen.AutoScroll = true;
            this.LevelScreen.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.LevelScreen.Location = new System.Drawing.Point(12, 49);
            this.LevelScreen.Name = "LevelScreen";
            this.LevelScreen.Size = new System.Drawing.Size(850, 600);
            this.LevelScreen.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Level Screen";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(869, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Tiles Screen";
            // 
            // Menu
            // 
            this.Menu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.File,
            this.ExportJson});
            this.Menu.Location = new System.Drawing.Point(0, 0);
            this.Menu.Name = "Menu";
            this.Menu.Size = new System.Drawing.Size(1479, 24);
            this.Menu.TabIndex = 4;
            this.Menu.Text = "menuStrip1";
            // 
            // File
            // 
            this.File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LoadTextures});
            this.File.Name = "File";
            this.File.Size = new System.Drawing.Size(37, 20);
            this.File.Text = "File";
            // 
            // LoadTextures
            // 
            this.LoadTextures.Name = "LoadTextures";
            this.LoadTextures.Size = new System.Drawing.Size(146, 22);
            this.LoadTextures.Text = "Load Textures";
            this.LoadTextures.Click += new System.EventHandler(this.LoadTextures_Click);
            // 
            // ExportJson
            // 
            this.ExportJson.Name = "ExportJson";
            this.ExportJson.Size = new System.Drawing.Size(78, 20);
            this.ExportJson.Text = "Export Json";
            this.ExportJson.Click += new System.EventHandler(this.ExportJson_Click);
            // 
            // listView
            // 
            this.listView.Location = new System.Drawing.Point(872, 107);
            this.listView.MultiSelect = false;
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(587, 516);
            this.listView.TabIndex = 5;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.Visible = false;
            this.listView.SelectedIndexChanged += new System.EventHandler(this.listView_SelectedIndexChanged);
            // 
            // tbxLevelName
            // 
            this.tbxLevelName.Location = new System.Drawing.Point(942, 633);
            this.tbxLevelName.Name = "tbxLevelName";
            this.tbxLevelName.Size = new System.Drawing.Size(100, 20);
            this.tbxLevelName.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(869, 636);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Level Name:";
            // 
            // cbxTileType
            // 
            this.cbxTileType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxTileType.FormattingEnabled = true;
            this.cbxTileType.Items.AddRange(new object[] {
            "DELETE",
            "Computers",
            "Doors",
            "DoorColliders",
            "EnemyBases",
            "Graunds",
            "Walls",
            "WallColliders",
            "Transporters"});
            this.cbxTileType.Location = new System.Drawing.Point(934, 49);
            this.cbxTileType.Name = "cbxTileType";
            this.cbxTileType.Size = new System.Drawing.Size(121, 21);
            this.cbxTileType.TabIndex = 15;
            this.cbxTileType.SelectedIndexChanged += new System.EventHandler(this.cbxTileType_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(869, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Tiles Type:";
            // 
            // tbxCollider
            // 
            this.tbxCollider.AutoSize = true;
            this.tbxCollider.Location = new System.Drawing.Point(88, 33);
            this.tbxCollider.Name = "tbxCollider";
            this.tbxCollider.Size = new System.Drawing.Size(61, 13);
            this.tbxCollider.TabIndex = 17;
            this.tbxCollider.Text = "Not Collider";
            // 
            // tbxRow
            // 
            this.tbxRow.Location = new System.Drawing.Point(204, 21);
            this.tbxRow.Name = "tbxRow";
            this.tbxRow.Size = new System.Drawing.Size(43, 20);
            this.tbxRow.TabIndex = 18;
            // 
            // tbxColumn
            // 
            this.tbxColumn.Location = new System.Drawing.Point(291, 21);
            this.tbxColumn.Name = "tbxColumn";
            this.tbxColumn.Size = new System.Drawing.Size(43, 20);
            this.tbxColumn.TabIndex = 19;
            // 
            // Row
            // 
            this.Row.AutoSize = true;
            this.Row.Location = new System.Drawing.Point(253, 25);
            this.Row.Name = "Row";
            this.Row.Size = new System.Drawing.Size(32, 13);
            this.Row.TabIndex = 20;
            this.Row.Text = "Row:";
            // 
            // Column
            // 
            this.Column.AutoSize = true;
            this.Column.Location = new System.Drawing.Point(155, 25);
            this.Column.Name = "Column";
            this.Column.Size = new System.Drawing.Size(45, 13);
            this.Column.TabIndex = 21;
            this.Column.Text = "Column:";
            // 
            // btnCreateLevel
            // 
            this.btnCreateLevel.Location = new System.Drawing.Point(350, 20);
            this.btnCreateLevel.Name = "btnCreateLevel";
            this.btnCreateLevel.Size = new System.Drawing.Size(75, 23);
            this.btnCreateLevel.TabIndex = 22;
            this.btnCreateLevel.Text = "Create Level";
            this.btnCreateLevel.UseVisualStyleBackColor = true;
            this.btnCreateLevel.Click += new System.EventHandler(this.btnCreateLevel_Click);
            // 
            // TileLabel
            // 
            this.TileLabel.AutoSize = true;
            this.TileLabel.Location = new System.Drawing.Point(1076, 57);
            this.TileLabel.Name = "TileLabel";
            this.TileLabel.Size = new System.Drawing.Size(35, 13);
            this.TileLabel.TabIndex = 23;
            this.TileLabel.Text = "label4";
            this.TileLabel.Visible = false;
            // 
            // tbxTile
            // 
            this.tbxTile.Location = new System.Drawing.Point(1166, 54);
            this.tbxTile.Name = "tbxTile";
            this.tbxTile.Size = new System.Drawing.Size(26, 20);
            this.tbxTile.TabIndex = 24;
            this.tbxTile.Visible = false;
            // 
            // butStartColliders
            // 
            this.butStartColliders.Location = new System.Drawing.Point(897, 149);
            this.butStartColliders.Name = "butStartColliders";
            this.butStartColliders.Size = new System.Drawing.Size(75, 23);
            this.butStartColliders.TabIndex = 25;
            this.butStartColliders.Text = "Start";
            this.butStartColliders.UseVisualStyleBackColor = true;
            this.butStartColliders.Visible = false;
            this.butStartColliders.Click += new System.EventHandler(this.butStartColliders_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1479, 669);
            this.Controls.Add(this.butStartColliders);
            this.Controls.Add(this.tbxTile);
            this.Controls.Add(this.TileLabel);
            this.Controls.Add(this.btnCreateLevel);
            this.Controls.Add(this.Column);
            this.Controls.Add(this.Row);
            this.Controls.Add(this.tbxColumn);
            this.Controls.Add(this.tbxRow);
            this.Controls.Add(this.tbxCollider);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cbxTileType);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbxLevelName);
            this.Controls.Add(this.listView);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LevelScreen);
            this.Controls.Add(this.Menu);
            this.DoubleBuffered = true;
            this.MainMenuStrip = this.Menu;
            this.Name = "Form1";
            this.Text = "Stellar Rats Level Generator";
            this.Menu.ResumeLayout(false);
            this.Menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.Panel LevelScreen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip Menu;
        private System.Windows.Forms.ToolStripMenuItem File;
        private System.Windows.Forms.ToolStripMenuItem LoadTextures;
        private System.Windows.Forms.ToolStripMenuItem ExportJson;
        private System.Windows.Forms.ListView listView;
        private System.Windows.Forms.TextBox tbxLevelName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbxTileType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label tbxCollider;
        private System.Windows.Forms.TextBox tbxRow;
        private System.Windows.Forms.TextBox tbxColumn;
        private System.Windows.Forms.Label Row;
        private System.Windows.Forms.Label Column;
        private System.Windows.Forms.Button btnCreateLevel;
        private System.Windows.Forms.Label TileLabel;
        private System.Windows.Forms.TextBox tbxTile;
        private System.Windows.Forms.Button butStartColliders;
    }
}

