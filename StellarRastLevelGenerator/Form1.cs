﻿using Newtonsoft.Json;
using StellarRats;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StellarRastLevelGenerator
{
    public partial class Form1 : Form
    {

        Level CurrentLevel;
        PictureBoxes currentPictureBox;
        static List<PictureBox> picBoxList = new List<PictureBox>();
        ImageList Imagelist;
        Image selectedImage;
        List<string> imageNames = new List<string>();
        string selectedName;
        int iRow,iColumn;
        float width_MAX, height_MAX;
        float width_Start, height_Start;
        public Form1()
        {
            InitializeComponent();
            CurrentLevel = new Level();
            string text = System.IO.File.ReadAllText(@".\Content\empty.json");
            CurrentLevel = JsonConvert.DeserializeObject<Level>(text);
            
        }

        private void btnCreateLevel_Click(object sender, EventArgs e)
        {
            {
                try
                {
                    iColumn = Convert.ToInt32(tbxColumn.Text);
                    iRow = Convert.ToInt32(tbxRow.Text);
                }
                catch
                {
                    MessageBox.Show("Please Entered Row and Column");
                }

                for (int i = 0; i < iRow; i++)
                {
                    for (int j = 0; j < iColumn; j++)
                    {
                        currentPictureBox = new PictureBoxes();
                        currentPictureBox.Size = new Size(50, 50);
                        currentPictureBox.Click += currentPictureBox_Click;
                        currentPictureBox.BorderStyle = BorderStyle.FixedSingle;
                        currentPictureBox.Location = new Point(i * 50, j * 50);
                        currentPictureBox.Coordinate_X = i * 50;
                        currentPictureBox.Coordinate_Y = j * 50;
                        currentPictureBox.Name = "picturebox";
                        currentPictureBox.Tag = "tag";
                        currentPictureBox.AccessibleName = "Not Collider";
                        picBoxList.Add(currentPictureBox);
                    }
                }

                for ( int i=0 ; i<picBoxList.Count; i++)
                {
                    LevelScreen.Controls.Add(picBoxList[i]);
                }
            }
        }


        //picturebox'a tıkladığında 
        void currentPictureBox_Click(object sender, EventArgs e)
        {
            var currPb = sender as PictureBoxes;
            Microsoft.Xna.Framework.Vector2 Selected_Vector = new Microsoft.Xna.Framework.Vector2();
            //MessageBox.Show(Convert.ToString(currPb.Location));
            if (bSelectingWallCollider)
            { 
                //todo add to collider
                currPb.AccessibleName = "Collider";
                Selected_Vector.X = currPb.Coordinate_X;
                Selected_Vector.Y = currPb.Coordinate_Y;
                currPb.BackColor = Color.Red;
                Vector_Wall_List.Add(Selected_Vector);
                return;
            }


            if (selectedImage != null && selectedName != null  )
            {
                if (cbxTileType.Text == "Computers" || cbxTileType.Text == "Doors" || cbxTileType.Text == "EnemyBases")
                {
                    if(tbxTile.Text != "")
                    {
                        currPb.Image = selectedImage;
                        currPb.Name = selectedName;
                        currPb.Tag = cbxTileType.Text;
                        tbxCollider.Text = currPb.AccessibleName;
                    }
                }
                else
                {
                    currPb.Image = selectedImage;
                    currPb.Name = selectedName;
                    currPb.Tag = cbxTileType.Text;
                    tbxCollider.Text = currPb.AccessibleName;
                }
                
            }
           
            ///////////////////////////////////////////////
            if (cbxTileType.Text == "Computers")
            {
                if(tbxTile.Text == "")
                {
                    MessageBox.Show("Please Enter the Computer ID!");
                }
                else
                {
                    if (selectedName != null)
                    {
                        MapComputer new_Computer = new MapComputer();
                        new_Computer.Width = 50;
                        new_Computer.Height = 50;
                        new_Computer.X = (int)currPb.Coordinate_X;
                        new_Computer.Y = (int)currPb.Coordinate_Y;
                        new_Computer.ID = Convert.ToInt32(tbxTile.Text);
                        CurrentLevel.Computer.Add(new_Computer);
                    }
                    else
                    {
                        MessageBox.Show("Please Choose Computer Tile!");
                    }
                }               
            }

            if (cbxTileType.Text == "Doors")
            {
                if (tbxTile.Text == "")
                {
                    MessageBox.Show("Please Enter the Doors ID!");
                }
                else
                {
                    if(selectedName != null)
                    {
                        MapDoors new_Door = new MapDoors();
                        new_Door.Width = selectedImage.Width;
                        new_Door.Height = selectedImage.Height;
                        new_Door.X = (int)currPb.Coordinate_X;
                        new_Door.Y = (int)currPb.Coordinate_Y;
                        new_Door.ID = Convert.ToInt32(tbxTile.Text);
                        CurrentLevel.Doors.Add(new_Door);
                    }
                    else
                    {
                        MessageBox.Show("Please Choose Door Tile!");
                    }
                    
                }
            }

            if (cbxTileType.Text == "EnemyBases")
            {
                if (tbxTile.Text == "")
                {
                    MessageBox.Show("Please Enter the EnemyBase ID!");
                }
                else
                {
                    if (selectedName != null)
                    {
                        MapEnemyBase new_EnemyBase = new MapEnemyBase();
                        new_EnemyBase.X = (int)currPb.Coordinate_X;
                        new_EnemyBase.Y = (int)currPb.Coordinate_Y;
                        new_EnemyBase.Type = currPb.Name;
                        new_EnemyBase.ID = Convert.ToInt32(tbxTile.Text);
                        CurrentLevel.EnemyBases.Add(new_EnemyBase);
                    }
                    else
                    {
                        MessageBox.Show("Please Choose EnemyBase Tile!");
                    }
                }
            }

            if (cbxTileType.Text == "Graunds")
            {
                if (selectedName != null)
                {
                    MapGrounds new_Graund = new MapGrounds();
                    new_Graund.X = (int)currPb.Coordinate_X;
                    new_Graund.Y = (int)currPb.Coordinate_Y;
                    CurrentLevel.Grounds.Add(new_Graund);
                }
                else
                {
                    MessageBox.Show("Please Choose Graunds Tile!");
                }
            }

            if (cbxTileType.Text == "Walls")
            {
                if (selectedName != null)
                {
                    MapWalls new_Wall = new MapWalls();
                    new_Wall.X = (int)currPb.Coordinate_X;
                    new_Wall.Y = (int)currPb.Coordinate_Y;
                    new_Wall.Type = currPb.Name;
                    CurrentLevel.Walls.Add(new_Wall);
                }
                else
                {
                    MessageBox.Show("Please Choose Walls Tile!");
                }
            }

            if (cbxTileType.Text == "Transporters")
            {
                if (selectedName != null)
                {
                    MapTransporter new_Tramsporter = new MapTransporter();
                    new_Tramsporter.X = (int)currPb.Coordinate_X;
                    new_Tramsporter.Y = (int)currPb.Coordinate_Y;
                    new_Tramsporter.Height = 50;
                    new_Tramsporter.Width = 50;
                    CurrentLevel.Transporters.Add(new_Tramsporter);
                }
                else
                {
                    MessageBox.Show("Please Choose Transporters Tile!");
                }
            }

            ///////////////////////////////////////////DELETE
            if(cbxTileType.Text == "DELETE")
            {
                if (currPb.Image != null)
                {
                    for (int i = 0; i < CurrentLevel.Computer.Count; i++)
                    {
                        if (CurrentLevel.Computer[i].X == currPb.Location.X && CurrentLevel.Computer[i].Y == currPb.Location.Y)
                        {
                            CurrentLevel.Computer.RemoveAt(i);
                        }
                    }
                    for (int i = 0; i < CurrentLevel.Doors.Count; i++)
                    {
                        if (CurrentLevel.Doors[i].X == currPb.Location.X && CurrentLevel.Doors[i].Y == currPb.Location.Y)
                        {
                            CurrentLevel.Doors.RemoveAt(i);
                        }
                    }
                    for (int i = 0; i < CurrentLevel.EnemyBases.Count; i++)
                    {
                        if (CurrentLevel.EnemyBases[i].X == currPb.Location.X && CurrentLevel.EnemyBases[i].Y == currPb.Location.Y)
                        {
                            CurrentLevel.EnemyBases.RemoveAt(i);
                        }
                    }
                    for (int i = 0; i < CurrentLevel.Grounds.Count; i++)
                    {
                        if (CurrentLevel.Grounds[i].X == currPb.Location.X && CurrentLevel.Grounds[i].Y == currPb.Location.Y)
                        {
                            CurrentLevel.Grounds.RemoveAt(i);
                        }
                    }
                    for (int i = 0; i < CurrentLevel.Walls.Count; i++)
                    {
                        if (CurrentLevel.Walls[i].X == currPb.Location.X && CurrentLevel.Walls[i].Y == currPb.Location.Y)
                        {
                            CurrentLevel.Walls.RemoveAt(i);
                        }
                    }
                    for (int i = 0; i < CurrentLevel.Transporters.Count; i++)
                    {
                        if (CurrentLevel.Transporters[i].X == currPb.Location.X && CurrentLevel.Transporters[i].Y == currPb.Location.Y)
                        {
                            CurrentLevel.Transporters.RemoveAt(i);
                        }
                    }
                    currPb.Image = null;
                    currPb.Name = "picturebox";
                    currPb.Tag = "tag";
                    currPb.AccessibleName = "Not Collider";
                }
            }
        }

        //Load textures tuşuna tıklandığında
        private void LoadTextures_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();
            try
            {
                DirectoryInfo dinfo = new DirectoryInfo(fbd.SelectedPath);
                Imagelist = new ImageList();
                
                String[] ImageFiles = Directory.GetFiles(fbd.SelectedPath);
                foreach (var file in ImageFiles)
                {

                    Imagelist.ImageSize = new Size(50, 50);
                    imageNames.Add(file.Split('\\').Last().Substring(0,2));
                    //Add images to Imagelist
                    Imagelist.Images.Add(Image.FromFile(file));
                }
                listView.LargeImageList = Imagelist;
                for (int i = 0; i < Imagelist.Images.Count; i++)
                {
                    listView.Items.Add(new ListViewItem() { ImageIndex = i });
                }
                MessageBox.Show("Tiles are Loaded");
            }
            catch
            {
            }
            
        }

        //Tile ekranında seçilen tile
        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbxTileType.Text == "")
            {
                MessageBox.Show("Please Selecet Tile Type!");
            }
            else
            {
                if (listView.SelectedIndices.Count > 0)
                {
                    var index = listView.SelectedIndices[0];
                    selectedImage = Imagelist.Images[index];
                    selectedName = imageNames[index];
                }
            }           
        }

        //Export json tuşuna basıldığında
        private void ExportJson_Click(object sender, EventArgs e)
        {
            
            if(tbxLevelName.Text == "")
            {
                MessageBox.Show("Please Enter the Level Name!");
            }
            else
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                DialogResult result = fbd.ShowDialog();      
                try
                {
                    CurrentLevel.sDescription = tbxLevelName.Text;
                    var JsonWrite = new StreamWriter(fbd.SelectedPath + "/" + tbxLevelName.Text + ".json");
                    var sJson = JsonConvert.SerializeObject(CurrentLevel, Formatting.Indented);
                    JsonWrite.Write(sJson);
                    JsonWrite.Close();
                }
                catch
                {
                }
            }           
        }
        
        private void cbxTileType_SelectedIndexChanged(object sender, EventArgs e)
        {
            TileLabel.Visible = false;
            tbxTile.Visible = false;
            //Combobox'da "Colliders" seçilirse
            if (cbxTileType.Text == "")
            {
                selectedImage = null;
                selectedName = null;
                butStartColliders.Visible = false;
                listView.Visible = false;
                TileLabel.Visible = false;
                tbxTile.Visible = false;
            }
            if (cbxTileType.Text == "Computers")
            {
                TileLabel.Text = "Computer ID: ";
                selectedImage = null;
                selectedName = null;
                butStartColliders.Visible = false;
                listView.Visible = true;
                TileLabel.Visible = true;
                tbxTile.Visible = true;
            }

            if (cbxTileType.Text == "WallColliders")
            {
                selectedImage = null;
                selectedName = null;
                butStartColliders.Visible = true;
                listView.Visible = false;
                TileLabel.Visible = false;
                tbxTile.Visible = false;
            }

            
            if (cbxTileType.Text == "Doors")
            {
                TileLabel.Text = "Door ID: ";
                selectedImage = null;
                selectedName = null;
                butStartColliders.Visible = false;
                listView.Visible = true;
                TileLabel.Visible = true;
                tbxTile.Visible = true;
            }
            if (cbxTileType.Text == "DoorColliders")
            {
                selectedImage = null;
                selectedName = null;
                butStartColliders.Visible = true;
                listView.Visible = false;
                TileLabel.Visible = false;
                tbxTile.Visible = false;

            }
            if (cbxTileType.Text == "EnemyBases")
            {
                TileLabel.Text = "EnemyBase ID: ";
                selectedImage = null;
                selectedName = null;
                butStartColliders.Visible = false;
                listView.Visible = true;
                TileLabel.Visible = true;
                tbxTile.Visible = true;
            }
            if (cbxTileType.Text == "Transporters")
            {
                TileLabel.Text = "Transporter ID: ";
                selectedImage = null;
                selectedName = null;
                butStartColliders.Visible = false;
                listView.Visible = true;
                TileLabel.Visible = false;
                tbxTile.Visible = false;
            }
            if (cbxTileType.Text == "Graunds")
            {
                selectedImage = null;
                selectedName = null;
                butStartColliders.Visible = false;
                listView.Visible = true;
                TileLabel.Visible = false;
                tbxTile.Visible = false;
            }
            if (cbxTileType.Text == "Walls")
            {
                selectedImage = null;
                selectedName = null;
                butStartColliders.Visible = false;
                listView.Visible = true;
                TileLabel.Visible = false;
                tbxTile.Visible = false;
            }
           
        }

        WallCollider SelectedWallCollider;
        List<Microsoft.Xna.Framework.Vector2> Vector_Wall_List = new List<Microsoft.Xna.Framework.Vector2>();
        bool bSelectingWallCollider = false;
        private void butStartColliders_Click(object sender, EventArgs e)
        {
            if (!bSelectingWallCollider)
            {
                
                butStartColliders.Text = "Save";
                bSelectingWallCollider = true;
            }
            else
            {
                //todo save collider

                CalculateColliders_Width();
                CalculateColliders_Height();
                SelectedWallCollider = new WallCollider();
                SelectedWallCollider.X = Convert.ToInt32(width_Start);
                SelectedWallCollider.Y = Convert.ToInt32(height_Start);
                SelectedWallCollider.Width = Convert.ToInt32(width_MAX);
                SelectedWallCollider.Height = Convert.ToInt32(height_MAX);
                CurrentLevel.WallColliders.Add(SelectedWallCollider);
                Vector_Wall_List.Clear();

                ////
                butStartColliders.Text = "Start";
                bSelectingWallCollider = false;
            
            }
        }

        //calculate wall colliders
        
        public void CalculateColliders_Width()
        {
            
            Microsoft.Xna.Framework.Vector2 currentVector;
            currentVector.X = 0;
            currentVector.Y = 0;

            //Küçükten büyüğe sıralıyoruz
            for (int i = 0; i < Vector_Wall_List.Count; i++)
            {
                for (int j = i + 1; j < Vector_Wall_List.Count; j++)
                {
                    if (Vector_Wall_List[j].X < Vector_Wall_List[i].X)
                    {
                        currentVector = Vector_Wall_List[j];
                        Vector_Wall_List[j] = Vector_Wall_List[i];
                        Vector_Wall_List[i] = currentVector;
                    }
                    if (Vector_Wall_List[j].Y < Vector_Wall_List[i].Y)
                    {
                        currentVector = Vector_Wall_List[j];
                        Vector_Wall_List[j] = Vector_Wall_List[i];
                        Vector_Wall_List[i] = currentVector;
                    }
                }
            }

            for (int i = 0; i < Vector_Wall_List.Count; i++)
            {
                
                for (int j = i + 1; j < Vector_Wall_List.Count; j++)
                {
                    if (Vector_Wall_List[i].X + 50 == Vector_Wall_List[j].X && Vector_Wall_List[i].Y == Vector_Wall_List[j].Y)
                    {

                        width_Start = Vector_Wall_List[0].X;
                        height_Start = Vector_Wall_List[0].Y;
                        width_MAX = Vector_Wall_List[j].X;
                        width_MAX = width_MAX + 50 - width_Start;
                        height_MAX = 50;                        
                    }
                }
            }

        }
        public void CalculateColliders_Height()
        {

            Microsoft.Xna.Framework.Vector2 currentVector;
            currentVector.X = 0;
            currentVector.Y = 0;

            //Küçükten büyüğe sıralıyoruz
            for (int i = 0; i < Vector_Wall_List.Count; i++)
            {
                for (int j = i + 1; j < Vector_Wall_List.Count; j++)
                {
                    if (Vector_Wall_List[j].X < Vector_Wall_List[i].X)
                    {
                        currentVector = Vector_Wall_List[j];
                        Vector_Wall_List[j] = Vector_Wall_List[i];
                        Vector_Wall_List[i] = currentVector;
                    }
                    if (Vector_Wall_List[j].Y < Vector_Wall_List[i].Y)
                    {
                        currentVector = Vector_Wall_List[j];
                        Vector_Wall_List[j] = Vector_Wall_List[i];
                        Vector_Wall_List[i] = currentVector;
                    }
                }
            }

            for (int i = 0; i < Vector_Wall_List.Count; i++)
            {

                for (int j = i + 1; j < Vector_Wall_List.Count; j++)
                {
                    if (Vector_Wall_List[i].Y + 50 == Vector_Wall_List[j].Y && Vector_Wall_List[i].X == Vector_Wall_List[j].X)
                    {

                        width_Start = Vector_Wall_List[0].X;
                        height_Start = Vector_Wall_List[0].Y;
                        height_MAX = Vector_Wall_List[j].Y;
                        height_MAX = height_MAX + 50 - height_Start;
                        width_MAX = 50;

                    }

                }
            }
        }
      
    }
}
